/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_int_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhumbert <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/13 14:17:06 by lhumbert          #+#    #+#             */
/*   Updated: 2021/07/13 14:25:42 by lhumbert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

enum	e_bool {false, true};
typedef enum e_bool	t_bool;

void	ft_sort_int_tab(int *tab, int size)
{
	t_bool	swaped;
	int		i;
	int		temp;

	i = 0;
	while (swaped || i > 0)
	{
		if (i == 0)
			swaped = false;
		if (tab[i + 1] < tab[i])
		{
			temp = tab[i];
			tab[i] = tab[i + 1];
			tab[i + 1] = temp;
			swaped = true;
		}
		if (i == size - 2)
			i = 0;
		else
			i++;
	}
}
